package com.example.counterapp

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var counterService: CounterService
    private var isBound = false
    private lateinit var textView: TextView

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as CounterService.CounterBinder
            counterService = binder.getService()
            isBound = true
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            isBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textView = findViewById(R.id.txt)

        val intent = Intent(this, CounterService::class.java)
        bindService(intent, connection, Context.BIND_AUTO_CREATE)

        val increment: Button = findViewById(R.id.increment)
        increment.setOnClickListener {
            counterService.increment()
        }

        val decrement: Button = findViewById(R.id.decrement)
        decrement.setOnClickListener {
            counterService.decrement()
        }

        val currentCounter: Button = findViewById(R.id.presentValue)
        currentCounter.setOnClickListener {
            getCurrentCounter()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isBound) {
            unbindService(connection)
            isBound = false
        }
    }

    private fun getCurrentCounter() {
        val currentCounter = counterService.counter
        textView.text = currentCounter.toString()
        Toast.makeText(this, "$currentCounter", Toast.LENGTH_SHORT).show()
    }
}