package com.example.counterapp

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder

class CounterService : Service() {

    private val binder = CounterBinder()
    var counter = 0
        private set

    inner class CounterBinder : Binder() {
        fun getService(): CounterService = this@CounterService
    }

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    fun increment() {
        counter++
    }

    fun decrement() {
        counter--
    }
}